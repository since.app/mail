// Package imports
const path = require('path').resolve
const Email = require('email-templates')
const nodemailer = require('nodemailer')

const { host, pass, user } = require('../.env')

module.exports = class Manager {
  constructor () {
    this.transport = nodemailer.createTransport({
      host,
      auth: { user, pass },
      port: 465,
      secure: true
    })

    this.email = new Email({
      message: {
        from: user
      },
      send: true,
      transport: this.transport
    })
  }

  async register (to, locals) {
    const name = locals.nickname ? this.ucname(locals.nickname) : locals.username
    return this.email.send({
      locals: { ...locals, name },
      message: typeof to === 'string' ? { to } : to,
      template: path(__dirname, 'register')
    })
  }

  ucname (name) {
    if (typeof name !== 'string' || !name.length) return ''
    return name[0].toUpperCase() + name.slice(1)
  }
}
